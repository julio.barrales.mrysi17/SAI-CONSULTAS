package lania.edu.mx.saiconsultas.control;

import java.util.List;
import lania.edu.mx.saiconsultas.datos.DaoVistaContrato;
import lania.edu.mx.saiconsultas.entidades.VistaContrato;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jcesa
 */
@RestController
@RequestMapping("/Contratos")
public class ControlVistaContrato {
     private final static Logger LOG = LoggerFactory.getLogger(ControlVistaContrato.class);
     @Autowired
     DaoVistaContrato vistacontrato;
     
     @GetMapping("/todos")
     public List<VistaContrato> getVistaContrato() {
         return vistacontrato.buscarTodos();
     }
}
