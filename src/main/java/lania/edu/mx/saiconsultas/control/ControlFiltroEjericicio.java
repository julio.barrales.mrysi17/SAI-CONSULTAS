/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.saiconsultas.control;

import java.util.List;
import lania.edu.mx.saiconsultas.entidades.Filtroejercicio;
import lania.edu.mx.saiconsultas.entidades.Filtroentidad;
import lania.edu.mx.saiconsultas.entidades.Filtromonto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lania.edu.mx.saiconsultas.datos.DaoFiltroEjercicio;
import lania.edu.mx.saiconsultas.datos.DaoFiltroEntidad;

@RestController
@RequestMapping("/ejercicio")
public class ControlFiltroEjericicio {
     private final static Logger LOG = LoggerFactory.getLogger(ControlConsultas.class);
     
     @Autowired
     DaoFiltroEjercicio daofiltrosejercicio;
     
     @GetMapping("/todos")
     public List<Filtroejercicio> getFiltroEjercicio() {
         return daofiltrosejercicio.buscarTodosEjercicio();
     }

}
