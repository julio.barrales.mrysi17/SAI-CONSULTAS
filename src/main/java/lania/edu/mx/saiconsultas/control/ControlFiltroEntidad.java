package lania.edu.mx.saiconsultas.control;

import java.util.List;
import lania.edu.mx.saiconsultas.datos.DaoFiltroEntidad;
import lania.edu.mx.saiconsultas.entidades.Filtroentidad;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/entidad")
public class ControlFiltroEntidad {
     private final static Logger LOG = LoggerFactory.getLogger(ControlFiltroEntidad.class);
     
     @Autowired
     DaoFiltroEntidad daofiltrosentidad;
     
     @GetMapping("/todos")
     public List<Filtroentidad> getFiltroEntidad() {
         return daofiltrosentidad.buscarTodosEntidad();
     }

}
