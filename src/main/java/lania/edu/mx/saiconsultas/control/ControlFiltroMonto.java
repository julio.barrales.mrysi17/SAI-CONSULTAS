package lania.edu.mx.saiconsultas.control;

import java.util.List;
import lania.edu.mx.saiconsultas.datos.DaoFiltroEntidad;
import lania.edu.mx.saiconsultas.datos.DaoFiltroMonto;
import lania.edu.mx.saiconsultas.entidades.Filtromonto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Monto")
public class ControlFiltroMonto {
     private final static Logger LOG = LoggerFactory.getLogger(ControlFiltroMonto.class);
     @Autowired
     DaoFiltroMonto daofiltrosmonto;
    
    @GetMapping("/todos")
    public List<Filtromonto> getFiltroMonto() {
         return daofiltrosmonto.buscarTodosMonto();
     }

}
