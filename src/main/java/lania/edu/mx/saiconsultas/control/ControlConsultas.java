package lania.edu.mx.saiconsultas.control;

import java.util.List;
import lania.edu.mx.saiconsultas.datos.DaoConsultas;
import lania.edu.mx.saiconsultas.entidades.Consulta;
import lania.edu.mx.saiconsultas.entidades.Filtroejercicio;
import lania.edu.mx.saiconsultas.entidades.Filtroentidad;
import lania.edu.mx.saiconsultas.entidades.Filtromonto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lania.edu.mx.saiconsultas.datos.DaoFiltroEjercicio;

@RestController
@RequestMapping("/consultas")
public class ControlConsultas {
    private final static Logger LOG = LoggerFactory.getLogger(ControlConsultas.class);
      
    @Autowired
    DaoConsultas daoconsultas;

    @GetMapping("")
    public List<Consulta> getTodos() {
        return daoconsultas.buscarTodos();
    }
   
    

}
