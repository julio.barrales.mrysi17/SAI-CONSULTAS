/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.saiconsultas.datos;

import java.util.List;
import lania.edu.mx.saiconsultas.entidades.Consulta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author jcesa
 */
public interface DaoConsultas extends JpaRepository<Consulta,Integer> {

    @Query("SELECT c FROM Consulta c")
    List<Consulta> buscarTodos();
    
}
