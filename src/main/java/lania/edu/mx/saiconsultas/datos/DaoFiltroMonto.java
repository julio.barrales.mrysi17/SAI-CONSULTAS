package lania.edu.mx.saiconsultas.datos;

import java.util.List;
import lania.edu.mx.saiconsultas.entidades.Filtromonto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DaoFiltroMonto extends JpaRepository<Filtromonto,Integer>{
    
        @Query("SELECT b FROM Filtromonto b")
        List<Filtromonto> buscarTodosMonto();

}
