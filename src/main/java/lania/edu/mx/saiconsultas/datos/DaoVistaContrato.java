package lania.edu.mx.saiconsultas.datos;

import java.util.List;
import lania.edu.mx.saiconsultas.entidades.VistaContrato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DaoVistaContrato extends JpaRepository<VistaContrato,Integer>{
    
            @Query("SELECT c FROM VistaContrato c")
            List<VistaContrato> buscarTodos();
}
