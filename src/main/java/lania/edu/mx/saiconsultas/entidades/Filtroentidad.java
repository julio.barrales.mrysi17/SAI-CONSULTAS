/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.saiconsultas.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name="FILTROENTIDAD")
public class Filtroentidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "filEntId")
    private Integer filEntId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "filEntTexto")
    private String filEntTexto;

    @Basic(optional = false)
    @Column(name = "filEntValor")
    private Integer filEntValor;

    public Filtroentidad() {
    }

    public Filtroentidad(Integer filEntId, String filEntTexto, Integer filEntValor) {
        this.filEntId = filEntId;
        this.filEntTexto = filEntTexto;
        this.filEntValor = filEntValor;
    }

    public Integer getFilEntId() {
        return filEntId;
    }

    public void setFilEntId(Integer filEntId) {
        this.filEntId = filEntId;
    }

    public String getFilEntTexto() {
        return filEntTexto;
    }

    public void setFilEntTexto(String filEntTexto) {
        this.filEntTexto = filEntTexto;
    }

    public Integer getFilEntValor() {
        return filEntValor;
    }

    public void setFilEntValor(Integer filEntValor) {
        this.filEntValor = filEntValor;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filEntId != null ? filEntId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filtroejercicio)) {
            return false;
        }
        Filtroentidad other = (Filtroentidad) object;
        if ((this.filEntId == null && other.filEntId != null) 
                || (this.filEntId != null 
                && !this.filEntId.equals(other.filEntId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidad [ id=" + filEntId + " ]";
    }

    
}
