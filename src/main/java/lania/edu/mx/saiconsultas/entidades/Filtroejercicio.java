package lania.edu.mx.saiconsultas.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcesa
 */
@Entity
@Table (name="FILTROEJERCICIO")
public class Filtroejercicio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "filEjeId")
    private Integer filEjeId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "filEjeTexto")
    private String filEjeTexto;

    @Basic(optional = false)
    @Column(name = "filEjeValor")
    private Integer filEjeValor;

    public Filtroejercicio() {
    }

    public Filtroejercicio(Integer filEjeId, String filEjeTexto, Integer filEjeValor) {
        this.filEjeId = filEjeId;
        this.filEjeTexto = filEjeTexto;
        this.filEjeValor = filEjeValor;
    }

    public Integer getFilEjeId() {
        return filEjeId;
    }

    public void setFilEjeId(Integer filEjeId) {
        this.filEjeId = filEjeId;
    }

    public String getFilEjeTexto() {
        return filEjeTexto;
    }

    public void setFilEjeTexto(String filEjeTexto) {
        this.filEjeTexto = filEjeTexto;
    }

    public Integer getFilEjeValor() {
        return filEjeValor;
    }

    public void setFilEjeValor(Integer filEjeValor) {
        this.filEjeValor = filEjeValor;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filEjeId != null ? filEjeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filtroejercicio)) {
            return false;
        }
        Filtroejercicio other = (Filtroejercicio) object;
        if ((this.filEjeId == null && other.filEjeId != null) 
                || (this.filEjeId != null 
                && !this.filEjeId.equals(other.filEjeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ejercico [ id=" + filEjeId + " ]";
    }

}
