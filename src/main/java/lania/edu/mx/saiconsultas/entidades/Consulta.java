/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.saiconsultas.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcesar23mx
 */
@Entity
@Table (name="CONSULTAS")
public class Consulta implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CONID")
    private Integer conid;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONNOMBRE")
    private String connombre;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONTITULO")
    private String contitulo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONDESCRI")
    private String condescri;

    public Consulta() {
    }

    public Consulta(Integer conid, String connombre, String contitulo, String condescri) {
        this.conid = conid;
        this.connombre = connombre;
        this.contitulo = contitulo;
        this.condescri = condescri;
    }

    public Integer getConid() {
        return conid;
    }

    public void setConid(Integer conid) {
        this.conid = conid;
    }

    public String getConnombre() {
        return connombre;
    }

    public void setConnombre(String connombre) {
        this.connombre = connombre;
    }

    public String getContitulo() {
        return contitulo;
    }

    public void setContitulo(String contitulo) {
        this.contitulo = contitulo;
    }

    public String getCondescri() {
        return condescri;
    }

    public void setCondescri(String condescri) {
        this.condescri = condescri;
    }

     @Override
    public int hashCode() {
        int hash = 0;
        hash += (conid != null ? conid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.conid == null && other.conid != null) || (this.conid != null && !this.conid.equals(other.conid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Curso[ id=" + conid + " ]";
    }
    
}
