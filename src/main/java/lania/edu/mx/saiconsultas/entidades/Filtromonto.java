/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.saiconsultas.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name="FILTROMONTO")
public class Filtromonto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "filMonId")
    private Integer filMonId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "filMonTexto")
    private String filMonTexto;

    @Basic(optional = false)
    @Column(name = "filMonValor")
    private Integer filMonValor;

    public Filtromonto() {
    }

    public Filtromonto(Integer filMonId, String filMonTexto, Integer filMonValor) {
        this.filMonId = filMonId;
        this.filMonTexto = filMonTexto;
        this.filMonValor = filMonValor;
    }

    public Integer getFolMonId() {
        return filMonId;
    }

    public void setFolMonId(Integer filMonId) {
        this.filMonId = filMonId;
    }

    public String getFolMonTexto() {
        return filMonTexto;
    }

    public void setFolMonTexto(String filMonTexto) {
        this.filMonTexto = filMonTexto;
    }

    public Integer getFolMonValor() {
        return filMonValor;
    }

    public void setFolMonValor(Integer filMonValor) {
        this.filMonValor = filMonValor;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filMonId != null ? filMonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filtroejercicio)) {
            return false;
        }
        Filtromonto other = (Filtromonto) object;
        if ((this.filMonId == null && other.filMonId != null) 
                || (this.filMonId != null 
                && !this.filMonId.equals(other.filMonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Monto [ id=" + filMonId + " ]";
    }
    
}
