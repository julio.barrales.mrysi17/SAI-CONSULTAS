
package lania.edu.mx.saiconsultas.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcesa
 */
@Entity
@Table(name = "VISTACONTRATOS")
public class VistaContrato  {
    
    @Id
    @Basic(optional = false)
    @Column(name = "CONTID")
    private Integer contId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONTNUMCONTRATO")
    private String contNumContrato;

    @Basic(optional = false)
    @Column(name = "CONTENTIDAD")
    private Integer conEntidad;
    
    @Basic(optional = false)
    @Column(name = "CONTEJERCICIO")
    private Integer conEjercicio;

    @Basic(optional = false)
    @Column(name = "CONTMONTOOTOR")
    private Double conMontoOtor;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "FILENTTEXTO")
    private String filEntTexto;

    public VistaContrato(Integer contId, String contNumContrato, Integer conEntidad, Integer conEjercicio, Double conMontoOtor, String filEntTexto) {
        this.contId = contId;
        this.contNumContrato = contNumContrato;
        this.conEntidad = conEntidad;
        this.conEjercicio = conEjercicio;
        this.conMontoOtor = conMontoOtor;
        this.filEntTexto = filEntTexto;
    }

    public VistaContrato(Integer contId, String contNumContrato, Integer conEntidad, Integer conEjercicio, Double conMontoOtor) {
        this.contId = contId;
        this.contNumContrato = contNumContrato;
        this.conEntidad = conEntidad;
        this.conEjercicio = conEjercicio;
        this.conMontoOtor = conMontoOtor;
    }

    public VistaContrato() {
    }

    public Integer getContId() {
        return contId;
    }

    public void setContId(Integer contId) {
        this.contId = contId;
    }

    public String getContNumContrato() {
        return contNumContrato;
    }

    public void setContNumContrato(String contNumContrato) {
        this.contNumContrato = contNumContrato;
    }

    public Integer getConEntidad() {
        return conEntidad;
    }

    public void setConEntidad(Integer conEntidad) {
        this.conEntidad = conEntidad;
    }

    public Integer getConEjercicio() {
        return conEjercicio;
    }

    public void setConEjercicio(Integer conEjercicio) {
        this.conEjercicio = conEjercicio;
    }

    public Double getConMontoOtor() {
        return conMontoOtor;
    }

    public void setConMontoOtor(Double conMontoOtor) {
        this.conMontoOtor = conMontoOtor;
    }

    public String getFilEntTexto() {
        return filEntTexto;
    }

    public void setFilEntTexto(String filEntTexto) {
        this.filEntTexto = filEntTexto;
    }
    
    
}
