/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lania.edu.mx.saiconsultas.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name="CONTRATOSOTORGADOS")
public class Contratosotorgados {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "contId")
    private Integer contId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contNumContrato")
    private String contNumContrato;

    @Basic(optional = false)
    @Column(name = "contEntidad")
    private Integer contEntidad;

    @Basic(optional = false)
    @Column(name = "contEjercicio")
    private Integer contEjercicio;
    
    @Basic(optional = false)
    @Column(name = "contMontoOtor")
    private Integer contMontoOtor;

    public Contratosotorgados() {
    }

    public Contratosotorgados(Integer contId, String contNumContrato, Integer contEntidad, Integer contEjercicio, Integer contMontoOtor) {
        this.contId = contId;
        this.contNumContrato = contNumContrato;
        this.contEntidad = contEntidad;
        this.contEjercicio = contEjercicio;
        this.contMontoOtor = contMontoOtor;
    }

    public Integer getContId() {
        return contId;
    }

    public void setContId(Integer contId) {
        this.contId = contId;
    }

    public String getContNumContrato() {
        return contNumContrato;
    }

    public void setContNumContrato(String contNumContrato) {
        this.contNumContrato = contNumContrato;
    }

    public Integer getContEntidad() {
        return contEntidad;
    }

    public void setContEntidad(Integer contEntidad) {
        this.contEntidad = contEntidad;
    }

    public Integer getContEjercicio() {
        return contEjercicio;
    }

    public void setContEjercicio(Integer contEjercicio) {
        this.contEjercicio = contEjercicio;
    }

    public Integer getContMontoOtor() {
        return contMontoOtor;
    }

    public void setContMontoOtor(Integer contMontoOtor) {
        this.contMontoOtor = contMontoOtor;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contId != null ? contId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filtroejercicio)) {
            return false;
        }
        Contratosotorgados other = (Contratosotorgados) object;
        if ((this.contId == null && other.contId != null) 
                || (this.contId != null 
                && !this.contId.equals(other.contId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Contrato [ id=" + contId + " ]";
    }
    
}
