<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appConsultas">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consultas SAI</title>
        <link rel="stylesheet" type="text/css" href="./Recursos/cajasconsultas.css">
        <link rel="stylesheet" type="text/css" href="./Recursos/estilos.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <script src="./js/ctlConsultas.js"></script>
    </head>
    <body>
        <%@ include file="./WEB-INF/jspf/menu.jspf"%>
        
        <h1>Datos abiertos</h1>
        <div ng-controller="CtrlConsultas">
            <div id="forma">
                    <div ng-repeat="a in listaConsultas" class="dataItem">
                        <a href="filtros.jsp">
                        <div clas="itemContent" >
                            <div class="titulo">
                                <h3>{{a.contitulo}}</h3>
                            </div>
                            <div class="description">
                                {{a.condescri}}
                            </div>
                        </div>
                        </a>
                    </div>
            </div>
        </div>
         <jsp:include page="./WEB-INF/jspf/pie.jspf" flush="true"/>
        
    </body>
</html>
<a href="fonts/poppins/Poppins-Black.ttf"></a>