var appFiltros = angular.module('appFiltros',[]);

appFiltros.controller('CtrlFiltros',['$scope',
                                     '$http',
                                     '$log',
    function($scope,$http,$log){
        $log.debug('Definiendo controlador-->');
        //Definimos variables
        
        $scope.listaFiltroEjercicio = []; // Lista de ejercicios 
        $scope.listaFiltroEntidades = []; // Lista de entidades
        $scope.listaFiltroMonto = []; // Lista de montos
        $scope.listaContrato = []; // Lista de contratos
        
        $scope.cargaFiltroEjercicio = function() {
            $http.get('ejercicio/todos')
                    .then(function(respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug('Ejercicio:' + respuesta.data);
                        $scope.listaFiltroEjercicio = respuesta.data;
                    });
        };
        $scope.cargaFiltroEntidad = function() {
            $http.get('entidad/todos')
                    .then(function(respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug('Entidad:' + respuesta.data);
                        $scope.listaFiltroEntidad = respuesta.data;
                    });
        };
        $scope.cargaFiltroMonto = function() {
            $http.get('Monto/todos')
                    .then(function(respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug('Monto:' + respuesta.data);
                        $scope.listaFiltroMonto = respuesta.data;
                    });
        };

        $scope.cargaContratos = function() {
            $http.get('Contratos/todos')
                    .then(function(respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug('Contrato:' + respuesta.data);
                        $scope.listaContrato = respuesta.data;
                    });
        };

        $scope.cargaFiltroEjercicio();
        $scope.cargaFiltroEntidad();
        $scope.cargaFiltroMonto();
        $scope.cargaContratos();
        
        $log.debug("<--Control definido");
        
    }
]);
