var appConsultas = angular.module('appConsultas',[]);

appConsultas.controller('CtrlConsultas',['$scope',
                                     '$http',
                                     '$log',
    function($scope,$http,$log){
        $log.debug('Definiendo controlador-->');
        //Definimos variables
        $scope.listaConsultas = []; // Lista de Consultas 
        
        $scope.cargaConsultas = function() {
            $http.get('consultas')
                    .then(function(respuesta) {
                        $log.debug(respuesta.data);
                        $log.debug('Consultas:' + respuesta.data);
                        $scope.listaConsultas = respuesta.data;
                    });
        };
        $scope.cargaConsultas();
        $log.debug("<--Control definido");
        
    }
]);
