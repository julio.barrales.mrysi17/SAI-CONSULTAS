<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./Recursos/estilos.css">
        <title>Agregar alumno</title>

    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/menu.jspf"%>
        <h2>Agregar Alumno</h2>
        <div style="overflow:hidden; "  >
        <form:form method="POST" 
        modelAttribute="alumno" 
        action="${pageContext.request.contextPath}/guardarAlumnos">
            <div>
                    <table align="center" width="400px">
                        <tr class="trh">
                            <td class="tdh"></td>
                            <td colspan="2" class="tdh">
                                <form:input path="id" value="${id}" />
                            <td></td>
                        </tr>
                        <tr class="trh">
                            <td class="tdh"> Matrícula:</td>
                            <td colspan="2" class="tdh"> 
                                 <form:input path="strMatricula" value="" />
                            </td>
                            <td></td>
                        </tr>
                        <tr class="trh">
                            <td class="tdh"> Nombres: </td>
                            <td colspan="2" class="tdh"> 
                                 <form:input path="strNombre" value="" />
                            </td>
                            <td></td>
                        </tr>
                        <tr class="trh">
                            <td class="tdh"> Apellidos: </td>
                            <td colspan="2" class="tdh"> 
                                 <form:input path="strApellidos" value="" />
                            </td>
                            <td></td>
                        </tr>
                        <tr class="trh">
                            <td class="tdh"> CURP:  </td>
                            <td colspan="2" class="tdh"> 
                                 <form:input path="strCurp" value="" />
                            </td>
                            <td></td>
                        </tr>
                        <tr class="trh">
                            <td class="tdh">  Fecha de ingreso:  </td>
                            <td colspan="2" class="tdh"> 
                                 <form:input type="date" path="fechaIngresoStr" value="" />   
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><input class="btn info" type="submit" value="Guardar"/></td>
                            <td><input class="btn danger" type="button" value="Cancelar" onclick="window.history.back();"/></td>
                            <td></td>
                        </tr>
                    </table>
            </div>
        </form:form>
        </div>
        <%@ include file="/WEB-INF/jspf/pie.jspf"%>
    </body>
</html>
