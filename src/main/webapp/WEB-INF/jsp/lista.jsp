<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appAlumnos">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <script src="js/ctlAlumnos.js"></script>
        <link rel="stylesheet" type="text/css" href="./Recursos/estilos.css">
        <title>Alumnos</title>
    </head>
    <body>
        <h1>Alumnos</h1>
         <%@ include file="/WEB-INF/jspf/menu.jspf"%>
         <div ng-controller="CtrlAlumnos">
            
            <a href="<%= getPath() %>/nuevoAlumno" class="btn success">Agregar</a>
               <p>&nbsp;</p>

               <table>
                   <thead>
                       <tr class="vtr">
                           <th class="vth">ID</th>
                           <th class="vth">MATRICULA</th>
                           <th class="vth">NOMBRE</th>
                           <th class="vth">APELLIDOS</th>
                           <th class="vth">CURP</th>
                           <th class="vth">FECHA DE INGRESO</th>
                           <th></th>
                       </tr>
                   </thead>
                    <tr ng-repeat="a in listaAlumnos" class="vtr">
                       <td class="vtd">{{a.id}}</td>
                       <td class="vtd">{{a.strMatricula}}</td>
                       <td class="vtd">{{a.strNombre}}</td>
                       <td class="vtd">{{a.strApellidos}}</td>
                       <td class="vtd">{{a.strCurp}}</td>
                       <td class="vtd">{{a.fechaIngreso | date: "dd/MM/yyyy"}} </td>
                       <td class="vtd">
                           <a class="btn info" href="<%= getPath() %>/editarAlumno?id=${a.id}" >Editar</a><br><br>
                           <a class="btn danger" href="<%= getPath() %>/borrarAlumno?id=${a.id}"  
                              onclick="return confirm('¿Estas seguro de Elimnar el registro?')">Borrar</a><br><br>
                       </td>
                   </tr>
               </table>
        </div>
            <jsp:include page="/WEB-INF/jspf/pie.jspf" flush="true"/>
    </body>
</html>
