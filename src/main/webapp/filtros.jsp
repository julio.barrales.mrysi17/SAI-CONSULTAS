<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appFiltros">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consultas SAI</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./Recursos/cajasconsultas.css">
        <link rel="stylesheet" type="text/css" href="./Recursos/estilos.css">
        <script src="./js/ctlFiltros.js"></script>
        <style>
            .contenido tr:nth-child(even) {background: #CCC !Important}
            .contenido tr:nth-child(odd) {background: #FFF !Important}
        </style>
    </head>
    <body>
        <%@ include file="./WEB-INF/jspf/menu.jspf"%>
        <h1>Datos abiertos</h1>
        <div ng-controller="CtrlFiltros">
            <table style="border: 0px">
                <tr>
                    <td>Ejercicio fiscal</td>
                    <td><select ng-model="ejercicio" ng-options="a.filEjeValor as a.filEjeTexto for a in listaFiltroEjercicio"></select></td>
                    <td></td>
                    <td>Entidad federativa</td>
                    <td><select ng-model="entidad" ng-options="a.filEntValor as a.filEntTexto for a in listaFiltroEntidad"></select></td>
               </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
               </tr>
                <tr>
                    <td>Monto otorgado</td>
                    <td><select ng-model="monto" ng-options="a.folMonValor as a.folMonTexto for a in listaFiltroMonto"></select></td>
                    <td></td>
                    <td><input type="button" value="Aplicar filtro"/></td>
                    <td><input type="button" value="Borrar filtro"/> </td>
               </tr>
            </table>
            <div class="contenido">
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Número contrato</th>
                            <th>Entidad</th>
                            <th>Ejercicio</th>
                            <th>Monto otorgado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="c in listaContrato">
                            <td>{{c.contId}}</td>
                            <td>{{c.contNumContrato}}</td>
                            <td>{{c.filEntTexto}}</td>
                            <td>{{c.conEjercicio}}</td>
                            <td>{{c.conMontoOtor | currency}}</td>
                        </tr>
                    </tbody>
                       
                </table>
            </div>
        </div> 
        <jsp:include page="./WEB-INF/jspf/pie.jspf" flush="true"/>
        
    </body>
</html>
<a href="fonts/poppins/Poppins-Black.ttf"></a>
